# spock

Web framework for rapid development inspired by Ruby's Sinatra.

See also [scotty](https://gitlab.com/haskell-packages-demo/scotty), maybe a bit more popular.
* https://repology.org/project/haskell:spock/versions
* https://repology.org/project/haskell:scotty/versions
